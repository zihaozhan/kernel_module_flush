#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/io.h>

void clearMva (unsigned long);

int
disable_cache(void)
{

  int i=0;
  //unsigned long readvalue = 0;
  unsigned long writeValue = 0;
  unsigned long t1 = 0;
  unsigned long t2 = 0;
  unsigned sum = 0;
  void *empty = NULL;
  unsigned long p_addr;
  unsigned size;
  // Start
  printk(KERN_INFO "Insert module \n");

  //p_addr = 0x3b40000;
  p_addr = 0xfffffff0;
  size = 0x10;
  empty = ioremap(p_addr, size);
  if (NULL == empty)
  {
    printk("ioremap 0x%lx error \n", p_addr);
    return 0;
  }

  printk("Physical: 0x%lx, Virtual: 0x%lx \n", p_addr, (unsigned long) empty);
  iounmap(empty); 
  
  // Set up PMU
  // enable PMC
  writeValue = 0x8000003f;
  asm volatile("mcr p15, 0, %0, c9, c12, 1":: "r" (writeValue));
  // Performance Control Register | 00111 | 
  // Always enable cycle counter | enable export events | 
  // disable clock divider | reset clock couner | reset event couner
  writeValue = 0x00000007;
  asm volatile("mcr p15, 0, %0, c9, c12, 0"::"r" (writeValue));
  
  for (i = 0; i < 100; ++i) {
    // Barrier
    asm volatile("dsb");
    asm volatile("isb");

    // Cycle Counter1
    asm volatile("mrc p15, 0, %[ts1], c9, c13, 0":[ts1] "=r" (t1));
    
    
    // Barier
    asm volatile("dsb");
    asm volatile("isb");

    //__raw_readl(empty);

    // Cycle Counter 2
    asm volatile("mrc p15, 0, %[ts2], c9, c13, 0":[ts2] "=r" (t2));
  
    sum += (t2 - t1);
  }
  printk("Cycle Counter Difference is %u \n", sum);
  return 0;
}
void
clearMva(unsigned long addr)
{
  asm volatile("mcr p15, 0, %0, c7, c14, 1"::"r" (addr));
}

void
enable_cache(void)
{
  printk(KERN_INFO "Remove module \n");
}

module_init(disable_cache);
module_exit(enable_cache);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("XXX");
MODULE_DESCRIPTION("Simple Example");
MODULE_VERSION("0.01");


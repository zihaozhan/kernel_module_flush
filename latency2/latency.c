#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/io.h>
#include <linux/random.h>

#define LAT_ARRAY_SIZE 8 * 1024 * 1024
#define LAT_LENGTH 22
void flushl1 (void);
void flushl2 (void);
void clearMva (unsigned long);

unsigned long target = 0;
char array[LAT_ARRAY_SIZE];

int
disable_cache(void)
{

  int i, j, k, l;
  unsigned sum = 0;
  unsigned long writeValue = 0;
  unsigned long t1 = 0;
  unsigned long t2 = 0;
  unsigned long temp = 0;
  unsigned long basis;
  unsigned long v_addr;
  unsigned long v_addr_0;
  unsigned long v_addr_1;
  unsigned diff_bit;
  unsigned diff_bit2;
  unsigned mask;
  //unsigned long p_addr;
  unsigned long random;
  // Start
  printk(KERN_INFO "Insert module \n");

  // Set up PMU
  // enable PMC
  writeValue = 0x8000003f;
  asm volatile("mcr p15, 0, %0, c9, c12, 1":: "r" (writeValue));
  // Performance Control Register | 00111 | 
  // Always enable cycle counter | enable export events | 
  // disable clock divider | reset clock couner | reset event couner
  writeValue = 0x00000007;
  asm volatile("mcr p15, 0, %0, c9, c12, 0"::"r" (writeValue));
  asm volatile("ldr %[dst], [%[addr]]"::[dst] "r" (temp), [addr] "r" (&target));
  
  // Initialize basis
  basis = (unsigned long) array;
  basis >>= LAT_LENGTH;
  basis += 1;
  basis <<= LAT_LENGTH;
  
  // Generate randomnumer
  for (l = 6; l < LAT_LENGTH; l++) {
    for (k = 6; k < LAT_LENGTH; k++) {
      diff_bit = k;
      diff_bit2 = l;
      mask = (1 << diff_bit) | (1 << diff_bit2);
      for (j = 0; j < 100; j++) {
        // Two address differs only in diff_bit
        get_random_bytes(&random, 4);
        random >>= (32 - LAT_LENGTH);
        v_addr = basis + random;
        v_addr_1 = v_addr | mask;
        v_addr_0 = v_addr & ~(mask);
        
        // Barrier
        asm volatile("dsb");
        asm volatile("isb");

        // Cycle Counter1 t1
        asm volatile("mrc p15, 0, %[ts1], c9, c13, 0":[ts1] "=r" (t1));

        for (i = 0; i < 1000; ++i) {
          // Load
          asm volatile("ldr %[d], [%[a]]"::[d] "r" (temp), [a] "r" (v_addr_0));
          asm volatile("ldr %[d], [%[a]]"::[d] "r" (temp), [a] "r" (v_addr_1));
          // Clear by VA
          clearMva(v_addr_0);
          clearMva(v_addr_1);
          // Barier
          asm volatile("dsb");
          asm volatile("isb");
        }
        // Cycle Counter 2 t2
        asm volatile("mrc p15, 0, %[ts2], c9, c13, 0":[ts2] "=r" (t2));
        sum += (t2 - t1);
      }
      sum /= 100000;
      printk("L(%2u, %2u): %u \t",diff_bit, diff_bit2, sum);
    }
    printk("\n");
  }
  return 0;
}

void
flushl1(void)
{
  unsigned int way;
  unsigned int set;
  unsigned int level;
  unsigned int mcrreg;
  mcrreg = 0;
  level = 0;
  for (set = 0; set < 256; set++) {
    for (way = 0; way < 4; way++) {
      mcrreg = (way << 30) + (set << 6) + (level << 1);
      asm volatile("mcr p15, 0, %0, c7, c14, 2"::"r" (mcrreg));
    }
  }
}

void
flushl2(void)
{
  unsigned int way;
  unsigned int set;
  unsigned int level;
  unsigned int mcrreg;
  mcrreg = 0;
  level = 1;
  for (set = 0; set < 2048; set++) {
    for (way = 0; way < 16; way++) {
      mcrreg = (way << 28) + (set << 6) + (level << 1);
      asm volatile("mcr p15, 0, %0, c7, c14, 2"::"r" (mcrreg));
    }
  }
}

void
clearMva(unsigned long addr)
{
  asm volatile("mcr p15, 0, %0, c7, c14, 1"::"r" (addr));
}

void
enable_cache(void)
{
  printk(KERN_INFO "Remove module \n");
}

module_init(disable_cache);
module_exit(enable_cache);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("XXX");
MODULE_DESCRIPTION("Simple Example");
MODULE_VERSION("0.01");


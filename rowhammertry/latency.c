#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/io.h>
#include <linux/random.h>

#define LAT_ARRAY_SIZE 8 * 1024 * 1024
#define LAT_LENGTH 22
#define LAT_ROW_L 16
#define LAT_ROW_BIT_NUM 6

void flushl1 (void);
void flushl2 (void);
void clearMva (unsigned long);

char array[LAT_ARRAY_SIZE];

int
disable_cache(void)
{

  int i, j, k;
  unsigned long t1 = 0;
  unsigned long t2 = 0;
  unsigned long temp = 0;
  unsigned long basis;
  unsigned long v_addr_base;
  unsigned long v_addr_0;
  unsigned long v_addr_2;
  unsigned row_num0 = 0;
  unsigned row_num2 = 2;
  unsigned mask;
  unsigned long random;
  // Start
  printk(KERN_INFO "Insert module \n");
 
  // Initialize basis
  basis = (unsigned long) array;
  basis >>= LAT_LENGTH;
  basis += 1;
  basis <<= LAT_LENGTH;
  
  mask = (1 << LAT_ROW_L);
  get_random_bytes(&random, 4);
  random >>= (32 - LAT_ROW_L);
  v_addr_base = basis + random;
  v_addr_0 = v_adrr_base + (row_num0 << LAT_ROW_L);
  v_addr_2 = v_adrr_base + (row_num2 << LAT_ROW_L);
  // Generate randomnumer
  for (k = 0; k < (1 << LAT_ROW_BIT_NUM); k++) {
    row_num0++;
    row_num2++;
    for (j = 0; j < 100; j++) {
      // Begin rowhammer
      for (i = 0; i < 1000; ++i) {
        // Load
        asm volatile("ldr %[d], [%[a]]"::[d] "r" (temp), [a] "r" (v_addr_0));
        asm volatile("ldr %[d], [%[a]]"::[d] "r" (temp), [a] "r" (v_addr_1));
        // Clear by VA
        clearMva(v_addr_0);
        clearMva(v_addr_1);
      }
    }
  }
  return 0;
}

void
flushl1(void)
{
  unsigned int way;
  unsigned int set;
  unsigned int level;
  unsigned int mcrreg;
  mcrreg = 0;
  level = 0;
  for (set = 0; set < 256; set++) {
    for (way = 0; way < 4; way++) {
      mcrreg = (way << 30) + (set << 6) + (level << 1);
      asm volatile("mcr p15, 0, %0, c7, c14, 2"::"r" (mcrreg));
    }
  }
}

void
flushl2(void)
{
  unsigned int way;
  unsigned int set;
  unsigned int level;
  unsigned int mcrreg;
  mcrreg = 0;
  level = 1;
  for (set = 0; set < 2048; set++) {
    for (way = 0; way < 16; way++) {
      mcrreg = (way << 28) + (set << 6) + (level << 1);
      asm volatile("mcr p15, 0, %0, c7, c14, 2"::"r" (mcrreg));
    }
  }
}

void
clearMva(unsigned long addr)
{
  asm volatile("mcr p15, 0, %0, c7, c14, 1"::"r" (addr));
}

void
enable_cache(void)
{
  printk(KERN_INFO "Remove module \n");
}

module_init(disable_cache);
module_exit(enable_cache);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("XXX");
MODULE_DESCRIPTION("Simple Example");
MODULE_VERSION("0.01");


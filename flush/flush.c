#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>


void flushl1 (void);
void flushl2 (void);
void clearMva (unsigned long);

unsigned long target = 0;
char array[2*4*1024];

int
disable_cache(void)
{

  int i=0;
  //unsigned long readvalue = 0;
  unsigned long writeValue = 0;
  unsigned long t1 = 0;
  unsigned long t2 = 0;
  unsigned long temp = 0;
  unsigned long mask = -1;
  unsigned sum = 0;
  // Start
  printk(KERN_INFO "Insert module \n");
  
  // Set up PMU
  // enable PMC
  writeValue = 0x8000003f;
  asm volatile("mcr p15, 0, %0, c9, c12, 1":: "r" (writeValue));
  // Performance Control Register | 00111 | 
  // Always enable cycle counter | enable export events | 
  // disable clock divider | reset clock couner | reset event couner
  writeValue = 0x00000007;
  asm volatile("mcr p15, 0, %0, c9, c12, 0"::"r" (writeValue));
  
  // set up L1-refill event counter
  // select counter : counter0
  writeValue = 0x00000000;
  asm volatile("mcr p15, 0, %0, c9, c12, 5"::"r" (writeValue));
  // select event : L1 cache refill
  writeValue = 0x00000003;
  asm volatile("mcr p15, 0, %0, c9, c13, 1"::"r" (writeValue));
  
  /*
  // disable L1-inst-cahe L1-data-cache L2-cache
  asm volatile("mrc p15, 0, %0, c1, c0, 0":"=r" (writeValue));
  printk("current control register is %lx \n", writeValue);
  mask = -1;
  //mask ^= 0x1004;
  mask ^= 0x0004;
  writeValue &= mask;
  asm volatile("mcr p15, 0, %0, c1, c0, 0"::"r" (writeValue));
  printk("now control register is %lx \n", writeValue);
  */

  asm volatile("ldr %[dst], [%[addr]]"::[dst] "r" (temp), [addr] "r" (&target));
  
  for (i = 0; i < 100; ++i) {
    // Flush o Clear
    //clearMva((unsigned long)&target);
    //flushl1();
    //flushl2();
    
    
    // Read Event Counter 1
    //asm volatile("mrc p15, 0, %0, c9, c13, 2":"=r" (readvalue));
    //printk("Cache refill counter is %lu \n", readvalue);
    
    // Barrier
    asm volatile("dsb");
    asm volatile("isb");
    // Cycle Counter1
    asm volatile("mrc p15, 0, %[ts1], c9, c13, 0":[ts1] "=r" (t1));
    
    // Load again
    asm volatile("ldr %[dst], [%[addr]]"::[dst] "r" (temp), [addr] "r" (&target));
    
    // Barier
    asm volatile("dsb");
    asm volatile("isb");
    // Cycle Counter 2
    asm volatile("mrc p15, 0, %[ts2], c9, c13, 0":[ts2] "=r" (t2));
  
    //printk("Timestamp1 is %lu \n", t1);
    //printk("Timestamp2 is %lu \n", t2);
    sum += (t2 - t1);
  }
  
  printk("Cycle Counter Difference is %u \n", sum);
  
  // Read Event Counter 2
  //asm volatile("mrc p15, 0, %0, c9, c13, 2":"=r" (readvalue));
  //printk("Cache refill counter is %lu \n", readvalue);

  return 0;
}

void
flushl1(void)
{
  unsigned int way;
  unsigned int set;
  unsigned int level;
  unsigned int mcrreg;
  mcrreg = 0;
  level = 0;
  for (set = 0; set < 256; set++) {
    for (way = 0; way < 4; way++) {
      mcrreg = (way << 30) + (set << 6) + (level << 1);
      asm volatile("mcr p15, 0, %0, c7, c14, 2"::"r" (mcrreg));
    }
  }
}

void
flushl2(void)
{
  unsigned int way;
  unsigned int set;
  unsigned int level;
  unsigned int mcrreg;
  mcrreg = 0;
  level = 1;
  for (set = 0; set < 2048; set++) {
    for (way = 0; way < 16; way++) {
      mcrreg = (way << 28) + (set << 6) + (level << 1);
      asm volatile("mcr p15, 0, %0, c7, c14, 2"::"r" (mcrreg));
    }
  }
}

void
clearMva(unsigned long addr)
{
  asm volatile("mcr p15, 0, %0, c7, c14, 1"::"r" (addr));
}

void
enable_cache(void)
{
  printk(KERN_INFO "Remove module \n");
}

module_init(disable_cache);
module_exit(enable_cache);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("XXX");
MODULE_DESCRIPTION("Simple Example");
MODULE_VERSION("0.01");

